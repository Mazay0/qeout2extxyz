#!/bin/sh

# convert QE pw.x log file to xyz trajectory with energy as a comment
# Usage example: 
# cat ZrO2_110_2_2.log  | ./qeout2extxyz.sh > ZrO2_110_2_2.xyz
#

cat | awk -v NEWLINE='\n' -- '
BEGIN {
	reading_intial_atoms=0
	reading_atoms=0
	reading_cell=0
	cell=""
	alat=0
	cur_energy="X3"
}

{
	if (reading_atoms > 0)
	{
		row = $0
		if (length(atomic_rows) == 0)
		{
			atomic_rows = row
		}
		else
		{
			atomic_rows = atomic_rows NEWLINE row
		}
		reading_atoms -= 1
	}

	if (reading_intial_atoms > 0)
	{
		row=sprintf("%-6s%14s%14s%14s", $2,  $7*alat,$8*alat,  $9*alat)
		if (length(atomic_rows) == 0)
		{
			atomic_rows = row
		}
		else
		{
			atomic_rows = atomic_rows NEWLINE row
		}
		reading_intial_atoms -= 1
	}

	if (reading_axes > 0)
	{
		ax[3-reading_axes] = $4*alat " " $5*alat " " $6*alat
		reading_axes -= 1
	}
	
	if (reading_cell > 0)
	{
		ax[3-reading_cell] = $1 " " $2 " " $3
		reading_cell -= 1
	}
}

/lattice parameter \(alat\)\s*=\s*[0-9\.]+/ {
	alat = 0.529179435125007 * $5
}

/crystal axes:/ {
	reading_axes=3
}

/CELL_PARAMETERS / {
	reading_cell=3
}


/positions \(alat units\)/ {
	reading_intial_atoms=natoms
	atomic_rows=""
}

/ATOMIC_POSITIONS/ {
	reading_atoms=natoms
	atomic_rows=""
} 

/!    total energy/ {
	cur_energy = $5
}

/number of atoms\/cell/ {
	natoms = $5
}

/Forces acting on atoms/ {
	print natoms
	print "E=" cur_energy " Lattice=\"" ax[0] " " ax[1] " " ax[2] "\"" " Properties=species:S:1:pos:R:3"
	print atomic_rows
}
'

